/**
 * @file
 * jQuery to make customizations to jquery_easy_ticker.
 */
(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.easyTickerFormAlter = {
    attach: function (context, settings) {
      if (settings.jqueryEasyTicker) {
        $(settings.jqueryEasyTicker).each(function (index, value) {
          $(value.pages).easyTicker({
            direction: 'up',
            easing: 'swing',
            speed: 'slow',
            interval: 2000,
            height: 'auto',
            visible: 0,
            mousePause: 1,
            controls: {
              up: '',
              down: '',
              toggle: '',
              playText: 'Play',
              stopText: 'Stop'
            }
          });
        });
      }
    }
  };
}(jQuery, Drupal));
