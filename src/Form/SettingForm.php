<?php

namespace Drupal\jquery_easy_ticker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration page for jQuery easy ticker settings.
 */
class SettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jquery_easy_ticker_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'jquery_easy_ticker.settings',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Implements jQuery easy ticker settings form.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('jquery_easy_ticker.settings');

    $header = [
      $this->t('ID/Class'),
      $this->t('Remove'),
    ];

    // Multi value table form.
    $form['ticker_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#prefix' => '<div id="fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $ticker_table = $form_state->get('ticker_table');
    if (empty($ticker_table)) {
      // Set data from configuration on page load.
      // Set empty element if no configurations are set.
      if (NULL !== $config->get('ticker_table')) {
        $ticker_table = $config->get('ticker_table');
        $form_state->set('ticker_table', $ticker_table);
      }
      else {
        $ticker_table = [''];
        $form_state->set('ticker_table', $ticker_table);
      }
    }

    // Create row for table.
    foreach ($ticker_table as $i => $value) {
      $form['ticker_table'][$i]['pages'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Pages'),
        '#title_display' => 'invisible',
        '#description' => $this->t('Add Class or ID, eg: #jquery_easy_ticker or .jquery_easy_ticker'),
        '#required' => TRUE,
        '#default_value' => isset($value['pages']) ? $value['pages'] : [],
      ];

      $form['ticker_table'][$i]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => "remove-" . $i,
        '#submit' => ['::removeElement'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::removeCallback',
          'wrapper' => 'fieldset-wrapper',
        ],
      ];
    }

    $form['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add more'),
      '#submit' => ['::addMore'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'fieldset-wrapper',
      ],
    ];

    $form_state->setCached(FALSE);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for ajax-enabled add buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['ticker_table'];
  }

  /**
   * Submit handler for the "Add one more" button.
   *
   * Add a blank element in table and causes a rebuild.
   */
  public function addMore(array &$form, FormStateInterface $form_state) {
    $ticker_table = $form_state->get('ticker_table');
    array_push($ticker_table, "");
    $form_state->set('ticker_table', $ticker_table);
    $form_state->setRebuild();
  }

  /**
   * Callback for ajax-enabled remove buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    return $form['ticker_table'];
  }

  /**
   * Submit handler for the "Remove" button(s).
   *
   * Remove the element from table and causes a form rebuild.
   */
  public function removeElement(array &$form, FormStateInterface $form_state) {
    // Get table.
    $ticker_table = $form_state->get('ticker_table');
    // Get element to remove.
    $remove = key($form_state->getValue('ticker_table'));
    // Remove element.
    unset($ticker_table[$remove]);
    // Set an empty element if no elements are left.
    if (empty($ticker_table)) {
      array_push($ticker_table, "");
    }
    $form_state->set('ticker_table', $ticker_table);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $data = $form_state->getValue('ticker_table');
    $key = 0;
    foreach ($data as $value) {
      if (strpos($value['pages'], '.') !== FALSE || strpos($value['pages'], '#') !== FALSE) {
      }
      else {
        $form_state->setErrorByName('ticker_table][' . $key, $this->t('Please add proper css Class aur ID.'));
      }
      $key++;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config('jquery_easy_ticker.settings')
      // Set the submitted configuration setting.
      ->set('ticker_table', $form_state->getValue('ticker_table'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
